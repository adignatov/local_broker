package org.fxclub.route;

import org.apache.activemq.advisory.AdvisorySupport;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.fxclub.broker.LocalBroker;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class TestRoute extends RouteBuilder {

    @Value("${queue.in}")
    String queueIn;

    @Value("${queue.out}")
    String queueOut;

    @Value("${queue.advisory}")
    String queueAdvisory;

    @Override
    public void configure() throws Exception {
        from("activemq:" + queueIn).to(LocalBroker.BROKER_NAME + ":queue:" + queueOut);
    }

}
