package org.fxclub.controller;

import org.fxclub.broker.LocalBroker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;


@RestController("/")
public class TestController {

    private LocalBroker localBroker;

    @Autowired
    public TestController(LocalBroker localBroker) {
        this.localBroker = localBroker;
    }

    /*
      size - size of message in kb
      count - message count
     */
    @RequestMapping(method = RequestMethod.GET, value = "/send_test_messages", params = {"size", "count"})
    public String sendTestMessages(
            @RequestParam(value = "size") int size,
            @RequestParam(value = "count") int count) {
        String s = "";
        for (int i = 0; i < size * 1024; i++) {
            s += "a";
        }

        for (int i = 0; i < count; i++) {
            localBroker.sendMessageToLocalBroker(s, new HashMap<>());
        }

        return "*** " + count + " messages sent ***";
    }

}
