package org.fxclub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmbeddedBrokerTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmbeddedBrokerTestApplication.class, args);
	}
}
