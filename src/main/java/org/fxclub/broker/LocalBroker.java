package org.fxclub.broker;

import com.oracle.tools.packager.Log;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.network.NetworkConnector;
import org.apache.activemq.store.kahadb.KahaDBPersistenceAdapter;
import org.apache.activemq.util.IOExceptionHandler;
import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.jms.ConnectionFactory;
import java.io.IOException;
import java.util.Map;

import static org.apache.camel.component.jms.JmsComponent.jmsComponentAutoAcknowledge;

@Service
public class LocalBroker {

    @Autowired
    ProducerTemplate producer;

    @Value("${activemq.connection.factory.username}")
    String remoteActivemqUser;

    @Value("${activemq.connection.factory.password}")
    String remoteActivemqPassword;

    @Value("${default.broker.url}")
    String remoteActivemqUrl;

    @Value("${queue.out}")
    String queueOut;

    @Value("${queue.advisory}")
    String queueAdvisory;

    public static final String BROKER_NAME = "fxlocal";


    @PostConstruct
    public void initBroker() throws Exception {
        //create new local broker
        BrokerService brokerService = new BrokerService();
        brokerService.setBrokerName(BROKER_NAME);
        brokerService.setPersistent(true);
        brokerService.setPersistenceAdapter(new KahaDBPersistenceAdapter());

        //add network connector
        NetworkConnector connector = brokerService.addNetworkConnector("static://" + remoteActivemqUrl);
        connector.setUserName(remoteActivemqUser);
        connector.setPassword(remoteActivemqPassword);
        connector.addStaticallyIncludedDestination(new ActiveMQQueue(queueOut));
        connector.addStaticallyIncludedDestination(new ActiveMQQueue(queueAdvisory));

        //start broker
        brokerService.start();

        //add broker component to the camel context
        if (producer.getCamelContext().getComponent(BROKER_NAME) == null) {
            ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("vm://" + BROKER_NAME);
            producer.getCamelContext().addComponent(BROKER_NAME, jmsComponentAutoAcknowledge(connectionFactory));
        }
    }

    public void sendMessageToLocalBroker(String body, Map headers) {
        producer.sendBodyAndHeaders(BROKER_NAME + ":queue:" + queueOut, body, headers);
    }

}
