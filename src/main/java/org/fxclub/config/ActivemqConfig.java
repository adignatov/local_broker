package org.fxclub.config;

import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.activemq.jms.pool.PooledConnectionFactory;
import org.apache.activemq.spring.ActiveMQConnectionFactory;
import org.apache.camel.component.jms.JmsConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;

/**
 * Configuration for ActiveMQ JMS connection pool.
 */
@Configuration
public class ActivemqConfig {
    @Value("${default.broker.url}")
    String defaultBrokerUrl;

    @Value("${activemq.connection.factory.username}")
    String userName;

    @Value("${activemq.connection.factory.password}")
    String password;

    @Value("${activemq.connection.factory.max_connections}")
    int maxConnections;

    @Value("${activemq.connection.factory.concurrent_consumers}")
    int concurrentConsumers;

    private ActiveMQConnectionFactory jmsConnectionFactory() {
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
        connectionFactory.setBrokerURL(defaultBrokerUrl);
        connectionFactory.setUserName(userName);
        connectionFactory.setPassword(password);
        connectionFactory.setTrustedPackages(Collections.singletonList("org.fxclub"));
        return connectionFactory;
    }

    @Bean(initMethod = "start", destroyMethod = "stop")
    public PooledConnectionFactory pooledConnectionFactory() {
        PooledConnectionFactory connectionFactory = new PooledConnectionFactory();
        connectionFactory.setMaxConnections(maxConnections);
        connectionFactory.setConnectionFactory(jmsConnectionFactory());
        return connectionFactory;
    }

    @Bean
    public JmsConfiguration jmsConfig() {
        JmsConfiguration configuration = new JmsConfiguration();
        configuration.setConnectionFactory(pooledConnectionFactory());
        configuration.setConcurrentConsumers(concurrentConsumers);
        return configuration;
    }

    @Bean
    public ActiveMQComponent activemq() {
        ActiveMQComponent component = new ActiveMQComponent();
        component.setConfiguration(jmsConfig());
        return component;
    }
}
